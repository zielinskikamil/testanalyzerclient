import { Component, View, bootstrap, provide, NgIf } from 'angular2/angular2';
import { TopBar } from './components/topBar/topbar';
import { HTTP_PROVIDERS } from 'angular2/http';
import { Inject } from 'angular2/angular2';
import { LocationStrategy, HashLocationStrategy, Route, AsyncRoute, Router, RouteData, RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, APP_BASE_HREF } from 'angular2/router';
import { Login } from './components/login/login';
import { Main } from './components/main/main';
import { Register } from './components/register/register';
import { Upload } from './components/upload/upload';
import { UploadTestRun } from './components/uploadTestRun/uploadTestRun';
import { UploadTestCycle } from './components/uploadTestCycle/uploadTestCycle';
import { AuthService } from './services/authService';
import { TestCycleService } from './services/testCycleService';
import { HttpService } from './services/httpService';
import { TestRunService } from './services/testRunService';
import { TestComparerService } from './services/testComparerService';
import { LoginData} from './common/models';
import { Http } from 'angular2/http'
import { Logout } from './components/logout/logout';
import { Compare } from './components/compare/compare';

@Component({
    selector: 'app'
})
@View({
    directives: [ROUTER_DIRECTIVES, TopBar, NgIf],
    templateUrl: 'app/home.html'
})

@RouteConfig([
    new Route({ path: '/', component: Main, name: 'Main' }),
    new Route({ path: '/register', component: Register, name: 'Register' }),
    new Route({ path: '/login', component: Login, name: 'Login' }),
    new Route({ path: '/upload', component: Upload, name: 'Upload' }),
    new Route({ path: '/upload/testRun', component: UploadTestRun, name: 'UploadTestRun' }),
    new Route({ path: '/upload/testCycle', component: UploadTestCycle, name: 'UploadTestCycle' }),
    new Route({ path: '/compare', component: Compare, name: 'Compare' }),
	new Route({path: '/logout', component: Logout, name: 'Logout' })
])
class AppComponent {
    constructor() {
        console.log('App Start');
    }
}

bootstrap(AppComponent,
    [
        ROUTER_PROVIDERS,
        provide(APP_BASE_HREF, { useValue: '/' }),
        provide(LocationStrategy, { useClass: HashLocationStrategy } ),
        HTTP_PROVIDERS,
        AuthService,
        TestCycleService,
        HttpService,
        TestRunService,
        TestComparerService
    ]);
