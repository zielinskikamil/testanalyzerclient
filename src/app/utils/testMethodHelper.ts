import { TestMethod } from '../common/models';

export class TestMethodHelper {
    static extractNameFromTestMethod(testMethod: TestMethod): string {
        return testMethod.ClassName + '.' + this.extractMethodSignatureFromTestMethod(testMethod);
    }

    static extractMethodSignatureFromTestMethod(testMethod: TestMethod): string {
        var methodParams = '';
        for (var i = 0; i < testMethod.MethodParameters.length; i++) {
            methodParams = methodParams + testMethod.MethodParameters[i].Name + '=' + testMethod.MethodParameters[i].Value;
            if (i + 1 !== testMethod.MethodParameters.length) {
                methodParams = methodParams + ', ';
            }
        }
        return testMethod.Name + '(' + methodParams + ')';
    }

    static countSuccessMethods(testMethods: TestMethod[]): number {
        return this.countOccurencesOfResultsInTestMethods(0, testMethods);
    }

    static countFailuredMethods(testMethods: TestMethod[]): number {
        return this.countOccurencesOfResultsInTestMethods(1, testMethods);
    }

    private static countOccurencesOfResultsInTestMethods(result: number, testMethods: TestMethod[]): number {
        var count: number = 0;
        for (var i = 0; i < testMethods.length; i++) {
            if (testMethods[i].Result === result) {
                count++;
            }
        }
        return count;
    }
}