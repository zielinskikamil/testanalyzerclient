import { Component, View, Input } from 'angular2/core';
import { NgClass } from 'angular2/common';
import { TestComparerService } from '../../../services/testComparerService';
import { Config } from '../../config/config';
import { MethodResultPipe } from '../../../common/testMethodResult.pipe';
import { MethodFullNamePipe } from '../../../common/testMethodFullName.pipe';
import { ParsedTestMethodRowFlafPipe } from '../../../common/parsedtestMethodRowFlag.pipe';

@Component({
    selector: 'compareGrid'
})

@View({
    directives: [NgClass],
    templateUrl: 'app/components/compare/compareGrid/view.html',
    pipes: [MethodResultPipe, MethodFullNamePipe, ParsedTestMethodRowFlafPipe]
})


export class CompareGrid {
    @Input() public testComparerService: TestComparerService;
    @Input() public config: Config;

    constructor() {
        console.log('Compare Grid Start');
    }
}