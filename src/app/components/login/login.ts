import { Component, View, CORE_DIRECTIVES, FORM_DIRECTIVES, NgIf} from 'angular2/angular2';
import { RouterLink, RouteParams } from 'angular2/router';
import { AuthService } from '../../services/authService';
import { ErrorBodyResponse, GenericResponse, LoginBodyRequest, AccessToken, AuthenticationErrorBodyResponse } from '../../common/models';

class LoginForm {
    email:string;
    password:string;
}

interface ILoginController {
    loginForm: LoginForm;
    errorlist: string[];
    isSuccess:boolean;
    login(event:any): void;
}

@Component({
    selector: 'login'
})

@View({
    directives: [CORE_DIRECTIVES, RouterLink, FORM_DIRECTIVES, NgIf],
    templateUrl: "app/components/login/view.html"
})

export class Login implements ILoginController {
    valid:boolean = true;
    loginForm:LoginForm = new LoginForm();
    errorlist:string[] = [];
    isSuccess:boolean = false;
    token:AccessToken = new AccessToken("", "", "", "", "", "");

    constructor(private authService:AuthService) {
        console.log('Login Start')

    }

    login():void {
        this.authService.login(this.convertToLoginBodyRequest())
            .subscribe(
                data => {
                console.log(data);
                this.handleResponse(data);
            },
            err => console.log('error ' + err),
            () => console.log('Register complete')
        );
    }

    private handleResponse(response:GenericResponse):void {
        if (response.status === 200) {
            console.log("OK - 200");
            this.isSuccess = true;
            try {
                var resp = JSON.parse(response._body);
                this.token.access_token = resp["access_token"],
                this.token.token_type = resp["token_type"],
                this.token.expires_in = resp["expires_in"],
                this.token.userName = resp["userName"],
                this.token.issued = resp[".issued"],
                this.token.expires = resp[".expires"]

                this.authService.handleToken(this.token);
            }
            catch (Error) {
                console.log("error while parsing response");
                this.errorlist.push("bład podczas przetwarzania odpowiedzi z serwera")
                this.isSuccess = false;
            }

        } else {
            console.log("httpstatusCode was = " + response.status);
            if (response._body) {
                var _body = JSON.parse(response._body)
                if (_body.error && _body.error_description) {
                    var authError:AuthenticationErrorBodyResponse = _body;
                    this.errorlist = ["Bład: " + authError.error + " Opis: " + authError.error_description];
                } else {
                    var body:ErrorBodyResponse = _body;
                    this.errorlist = body.ModelState[''];
                }
                console.log(body);

            }
        }
    }

    private convertToLoginBodyRequest():LoginBodyRequest {
        return new LoginBodyRequest(
            this.loginForm.email,
            this.loginForm.password
        );
    }

    private isStringExistInErrorList(msg:string):boolean {
        for (var i:number = 0; i < this.errorlist.length; i++) {
            if (this.errorlist[i] === msg) {
                return true;
            }
        }
        return false;
    }
}