import { Component, View } from 'angular2/angular2';
import { TestCycleService } from '../../services/testCycleService';
import { TestRunService } from '../../services/testRunService';
import { AuthService } from '../../services/authService'
import { ErrorBodyResponse, TestCycle, SelectOption, TestRun } from '../../common/models';
import { Upload } from '../upload/upload';

@Component({
    selector: 'uplaodTestRun'
})
@View({
    directives: [Upload],
    templateUrl: 'app/components/uploadTestCycle/view.html'
})
export class UploadTestCycle {
    public isSuccess:boolean = false;
    public errorlist:string[] = [];
    public name:string;
    public comment:string;
    public isApproved:boolean;

    constructor(private testCycleService:TestCycleService, private testRunService:TestRunService, private authService:AuthService) {
    }

    public upload() {
        this.errorlist = [];
        this.testCycleService.upload(this.name, this.isApproved, this.comment)
            .subscribe(
                data => {
                console.log('200 - OK!');
                this.isSuccess = true;
                },
                err => {
                var body:ErrorBodyResponse = JSON.parse(err);
                console.log("error occurred during uploading Test Cycle: " + JSON.stringify(body));
                this.errorlist.push(body.Message);
            },
            () => console.log('uploading testCycle completed')
        );
    }

    public isUserLoggedIn():boolean {
        return this.authService.isUserLoggedIn();
    }
}
