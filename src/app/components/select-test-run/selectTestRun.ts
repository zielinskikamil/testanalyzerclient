import { Component, View, CORE_DIRECTIVES, Input } from 'angular2/angular2';
import { TestCycleService } from '../../services/testCycleService';
import { TestRunService } from '../../services/testRunService';
import { TestComparerService } from '../../services/testComparerService';
import { SelectOption, TestCycle, TestRun } from '../../common/models';

interface ISelectTestRunController {
    fetchTestRuns(selectedTestCycleId:number): void;
    addChosenTestRun(): void;
}

@Component({
    selector: 'select-test-run'
})
@View({
    directives: [CORE_DIRECTIVES],
    templateUrl: "app/components/select-test-run/view.html"
})
export class SelectTestRun implements ISelectTestRunController {
    @Input() testComparerService: TestComparerService;

    public testCycleSelectOptions: SelectOption[];
    public testRunsSelectOptions: SelectOption[];
    public testCycleId: number;
    public testRunId: number;

    private testCycles: TestCycle[];
    private testRuns: TestRun[];

    constructor(public testCycleService:TestCycleService,
                public testRunService: TestRunService) {
        console.log('Select Test Run Start');

        var self: SelectTestRun = this;
         testCycleService.getTestCycle()
            .map(res => res.json())
            .subscribe(
                data => {
                    self.testCycles = data;
                    self.testCycleSelectOptions = data.map((testCycle: TestCycle) => {
                    return {
                        name: testCycle.Name,
                        id: testCycle.Id };
                    });
                },
                err => console.log('error ' + err),
                () => console.log('getting testCycles completed')
            );
    }

    public fetchTestRuns(selectedTestCycleId:number): void {
        var self: SelectTestRun = this;
        this.testRunService.getByTestCycleId(selectedTestCycleId)
            .map(res => res.json())
                .subscribe(
                    data => {
                        self.testRuns = data;
                        self.testRunsSelectOptions = data.map((testRun: TestRun) => {
                            return {
                                name: ((testRun.Name) ? testRun.Name : testRun.Comment ) + ' (' +  testRun.Id+')',
                                id: testRun.Id };
                            });
                    },
                    err => console.log('error ' + err),
                    () => console.log('getting testRuns completed')
                );
    }

    public addChosenTestRun(): void {
        var selectedTestRunId: number = this.testRunId;
        var chosenTestRun: TestRun = this.testRuns.find((value:TestRun):boolean => value.Id == selectedTestRunId);
        var testCycleOfChosenTestRun: TestCycle = this.testCycles[0]; // todo: we need test cylce where Test run belong. for now we do it in this way but it need to be fix
        chosenTestRun.TestCycle = testCycleOfChosenTestRun;
        this.testComparerService.addTestRun(chosenTestRun);
    }

    get diagnostic() { return 'Choose Test Run' + JSON.stringify(this.testComparerService); }

    get downloadedTestCycles() { return 'Donwloaded TestCycles' + JSON.stringify(this.testCycleSelectOptions); }
}