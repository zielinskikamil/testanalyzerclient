import { Http, Headers,Response, RequestOptionsArgs } from 'angular2/http';
import {Injectable, Component} from 'angular2/angular2';
import { Observable } from 'angular2/core';
import { AuthService } from './authService';

interface IHttpService {
    get(url: string, options?: RequestOptionsArgs): Observable<Response>;
    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response>;
    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response>;
    delete(url: string, options?: RequestOptionsArgs): Observable<Response>;
}

@Injectable()
export class HttpService implements IHttpService {
    constructor(private http:Http, private authService:AuthService) {
    }

    get(url:string, options:RequestOptionsArgs):Observable<Response> {
        this.setAuthHeader(options.headers);
        return this.http.get(url, options);
    }

    post(url:string, body:string, options:RequestOptionsArgs):Observable<Response> {
        this.setAuthHeader(options.headers);
        return this.http.post(url, body, options);
    }

    put(url:string, body:string, options:RequestOptionsArgs):Observable<Response> {
        this.setAuthHeader(options.headers);
        return this.http.put(url, body, options);
    }

    delete(url:string, options:RequestOptionsArgs):Observable<Response> {
        this.setAuthHeader(options.headers);
        return this.http.delete(url, options);
    }

    private setAuthHeader(headers:Headers) {
        if (this.authService.isUserLoggedIn()) {
            headers.append("Authorization", "Bearer " + this.authService.getToken().access_token);
        } else {
            console.log('cannot authorize, user is not logged in');
        }
    }

}