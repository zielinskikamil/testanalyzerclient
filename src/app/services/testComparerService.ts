import { TestRun } from '../common/models';
import { TestMethod } from '../common/models';
import { TestMethodHelper } from '../utils/testMethodHelper';

export class ParsedTestRun {
    constructor(
        public testRunId: any,
        public testCycleName: string,
        public testRunName: string,
        public comment: string,
        public successed: number,
        public failired: number,
        public other: number,
        public total: number,
        public testMethods: TestMethod[]) { }
}

export class ParsedTestMethodRow {
    public isAllFailed: boolean;
    public resultsOfMethod: ResultOfMethod[] = [];
    constructor(
        public testMethod: TestMethod,
        public isAllPass: boolean,
        public isNew: boolean,
        resultOfMethod: ResultOfMethod
    ) {
        this.resultsOfMethod.push(resultOfMethod);
    }
}

export class ResultOfMethod {
    result: string;
    duration: string;
    belongToTestRunWithId: number;
    isInvoked: boolean;
}

interface ITestComparerServiceController {
    parsedTestRun: ParsedTestRun[];
    parsedTestMethodRows: ParsedTestMethodRow[];
    addTestRun(testRun: TestRun): void;
}

export class TestComparerService implements ITestComparerServiceController {
    public parsedTestRun: ParsedTestRun[] = [];
    public parsedTestMethodRows: ParsedTestMethodRow[] = [];
    private testRuns: TestRun[] = [];

    constructor() {
        console.log('TestComparerService Start');
    }

    addTestRun(testRun: TestRun): void {
        this.testRuns.push(testRun);
        this.prepareTestRunCompare();
    }

    private prepareTestRunCompare(): void {
        // after each TestRun additions, we need to:
        // clean old values
        this.parsedTestRun = [];
        this.parsedTestMethodRows = [];

        // prepare summary of each Test Run (Table Header section)
        this.prepareSummaryOfTestRuns();
        this.prepareUniqTestMethodsMatrix();

        // -> mamy juz unikalne metody wraz z ich wynikikami.
        // teraz trzeba przeleciec przez te metody i zobaczyc czy mamy w kazdej po tyle samo resultatow co
        // test ranow - jesli nie to znaczy ze trzeba dodac (chyba puste komorki bo co innego? ) do tabeli aby wypelnic luki
        // i co najwazniejsze ustawic flagi ParsedTestMethodRow np gdy mamy wszedzie Passy to ustaw flage isAllPass na true, itp (trzeba rozwazyc kazdy case)
        this.handleMethodsNotInvokedInTestRun();
        this.setProperFlagOfEachRow();

        if (this.parsedTestRun.length > 1) {
            this.arrangeTestMethodInProperColumnOrder();
        }
        // -> podczas powyzszych operacji kolejnosc mogla sie przestawic,
        // dlatego trzeba dopasowac parsedTestRun.testRunId z ParsedTestMethodRow.resultsOfMethod.belongToTestRunWithId
    }

    private prepareSummaryOfTestRuns(): void {
        for (var i = 0; i < this.testRuns.length; i++) {
            // prepare summary of testRun
            var testRunTestCycleName: string = this.testRuns[i].TestCycle.Name;
            var testRunName: string = this.testRuns[i].Name;
            var testRunTestComment: string = this.testRuns[i].Comment;
            var testRunSuccess: number = TestMethodHelper.countSuccessMethods(this.testRuns[i].TestsMethods);
            var testRunFailures: number = TestMethodHelper.countFailuredMethods(this.testRuns[i].TestsMethods);
            var testRunTotal: number = this.testRuns[i].TestsMethods.length;
            var testRunOthers: number = testRunTotal - testRunSuccess - testRunFailures;

            var parsedTestRun: ParsedTestRun = new ParsedTestRun(
                this.testRuns[i].Id,
                testRunTestCycleName,
                testRunName,
                testRunTestComment,
                testRunSuccess,
                testRunFailures,
                testRunOthers,
                testRunTotal,
                this.testRuns[i].TestsMethods
            );

            this.parsedTestRun.push(parsedTestRun);
        }
    }

    private handleMethodsNotInvokedInTestRun(): void {
        for (var i = 0; i < this.parsedTestMethodRows.length; i++) {
            // gdy mamy 2 TestRany i a wynikow z danej metody 1 to
            var currentParsedRow = this.parsedTestMethodRows[i];
            if (currentParsedRow.resultsOfMethod.length != this.testRuns.length) {
                console.log('Method: ' + TestMethodHelper.extractNameFromTestMethod(currentParsedRow.testMethod) + 'doesnt exist in every Test Run');
                var resultsOfMethodToAdd: ResultOfMethod[] = [];
                for (var j = 0; j < this.testRuns.length; j++) { // tu jest np 3 test rany
                    var currentParsedTestRan = this.testRuns[j];
                    //patrzymy czy dana metoda wystepowala w danym test ranie
                    var isInvoked = currentParsedRow.resultsOfMethod.find(result => currentParsedTestRan.Id == result.belongToTestRunWithId);
                    // jesli nie wystepuje to dodaj pusta z obecnie parsowanego Test Runa
                    if (isInvoked == undefined) {
                        currentParsedRow.resultsOfMethod.push({
                            result: '',
                            duration: '',
                            belongToTestRunWithId: currentParsedTestRan.Id,
                            isInvoked: false
                        });
                    }
                }
            }
        }

        // tu wszytskie metodyRow powinny miec dokladnie 2 wykonanie
        for (var i = 0; i < this.parsedTestMethodRows.length; i++) {
            if (this.parsedTestMethodRows[i].resultsOfMethod.length != this.testRuns.length) {
                throw new Error("nieprawidłowe wypełnienie test run resultów w metodach metodami notInvoked");
            }
        }
    }

    private arrangeTestMethodInProperColumnOrder(): void {
        var sortedIdsOfParsedTestRuns: number[] = [];
        for (var i = 0; i < this.parsedTestRun.length; i++) {
            sortedIdsOfParsedTestRuns.push(this.parsedTestRun[i].testRunId);
        }
        console.log(sortedIdsOfParsedTestRuns);

        for (var i = 0; i < this.parsedTestMethodRows.length; i++) {
            var currentParsedRow: ParsedTestMethodRow = this.parsedTestMethodRows[i];
            currentParsedRow.resultsOfMethod = currentParsedRow.resultsOfMethod
                .sort((a: ResultOfMethod, b: ResultOfMethod) => {
                    var boolValue: boolean = (sortedIdsOfParsedTestRuns.indexOf(a.belongToTestRunWithId) > sortedIdsOfParsedTestRuns.indexOf(b.belongToTestRunWithId))

                    if (boolValue == true) {
                        return 1;
                    } else if (boolValue == false) {
                        return -11;
                    }

                    return 0;
                });
        }
    }

    private setProperFlagOfEachRow(): void {
        class Flag {
            public passCount: number = 0;
            public failCount: number = 0;
        }

        var maxResultsCount: number = this.testRuns.length;
        var flagsSummaryOfCurrentParsedRow: Flag;
        var currentParsedRow: ParsedTestMethodRow;

        for (var i = 0; i < this.parsedTestMethodRows.length; i++) {
            currentParsedRow = this.parsedTestMethodRows[i];
            flagsSummaryOfCurrentParsedRow = new Flag();

            for (var j = 0; j < currentParsedRow.resultsOfMethod.length; j++) {
                if (!currentParsedRow.resultsOfMethod[j].isInvoked) {
                    continue;
                }

                if (currentParsedRow.resultsOfMethod[j].result == "0") {
                    flagsSummaryOfCurrentParsedRow.passCount += 1;
                } else {
                    flagsSummaryOfCurrentParsedRow.failCount += 1;
                }
            }

            if (flagsSummaryOfCurrentParsedRow.passCount == maxResultsCount) {
                currentParsedRow.isAllPass = true;
                currentParsedRow.isAllFailed = false;
            } else if (flagsSummaryOfCurrentParsedRow.failCount == maxResultsCount) {
                currentParsedRow.isAllPass = false;
                currentParsedRow.isAllFailed = true;
            } else {
                currentParsedRow.isAllPass = false;
                currentParsedRow.isAllFailed = false;
            }
        }
    }

    private prepareUniqTestMethodsMatrix(): void {
        // przejedz przez kazda testowa metode w kazdym testranie.
        // sprawdz czy taka juz istnieje w parsedTestMethods property
        // jesli nie to ja dodaj
        // jesli tak to dodaj result tej metody do tablicy resultow
        for (var i = 0; i < this.parsedTestRun.length; i++) {
            var obecnieZParsowanyTestRun: ParsedTestRun = this.parsedTestRun[i];

            for (var j = 0; j < obecnieZParsowanyTestRun.testMethods.length; j++) {
                var testMethod: TestMethod = obecnieZParsowanyTestRun.testMethods[j];
                var alreadyAdded: ParsedTestMethodRow = this.checkIfAlreadyExistInParsedTestMethodRows(testMethod);
                if (alreadyAdded == undefined) {
                    // add new
                    console.log('Adding new ParsedTestMethodRow: ' + TestMethodHelper.extractNameFromTestMethod(testMethod));
                    var newParsedTestMethodRow: ParsedTestMethodRow = new ParsedTestMethodRow(
                        testMethod,
                        testMethod.Result,
                        true,
                        {
                            result: testMethod.Result,
                            duration: testMethod.Duration,
                            belongToTestRunWithId: obecnieZParsowanyTestRun.testRunId,
                            isInvoked: true
                        });
                    this.parsedTestMethodRows.push(newParsedTestMethodRow);
                } else {
                    // already exist
                    console.log('Adding one more result to method: ' + TestMethodHelper.extractNameFromTestMethod(alreadyAdded.testMethod));
                    alreadyAdded.resultsOfMethod.push({
                        result: testMethod.Result,
                        duration: testMethod.Duration,
                        belongToTestRunWithId: obecnieZParsowanyTestRun.testRunId,
                        isInvoked: true
                    });
                };
            }
        }
    }

    private checkIfAlreadyExistInParsedTestMethodRows(testMethod): ParsedTestMethodRow {
        for (var i = 0; i < this.parsedTestMethodRows.length; i++) {
            if (TestMethodHelper.extractNameFromTestMethod(this.parsedTestMethodRows[i].testMethod) === TestMethodHelper.extractNameFromTestMethod(testMethod)) {
                return this.parsedTestMethodRows[i];
            }
        }

        return undefined;
    }
}
