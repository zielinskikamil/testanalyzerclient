import {Injectable, Component} from 'angular2/angular2';
import { Headers } from 'angular2/http';
import { Constants } from '../utils/constants';
import { GenericResponse, AccessToken } from '../common/models';
import { AuthService } from './authService';
import { HttpService } from './httpService';

interface ITestCycleService {
    getTestCycle(): any;
}

@Injectable()
export class TestCycleService implements ITestCycleService {

    constructor(private authService: AuthService, private http: HttpService) {
    }

    getTestCycle(): any {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.get(Constants.BASE_URL + 'api/TestCycle', { headers: headers });
    }

    upload(name:string, isApproved: boolean, comment?: string ): any {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(Constants.BASE_URL + 'api/TestCycle', JSON.stringify({'TestRuns': [], 'name':name, 'comment': comment != null ? comment : '',
            'User':  this.authService.isUserLoggedIn() ? this.authService.getToken().access_token : {},
            'isApproved': isApproved}), {'headers': headers});
    }
}
