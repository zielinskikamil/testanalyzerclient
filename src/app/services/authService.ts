import {Injectable, Inject} from 'angular2/angular2';
import {Http, Headers } from 'angular2/http';
import { Constants } from '../utils/constants';
import { LoginBodyRequest, GenericResponse, RegisterBodyRequest, AccessToken } from '../common/models';
import {Router} from 'angular2/router';


interface IAuthController {
    login(loginBodyRequest:LoginBodyRequest): any;
    register(registerBodyRequest:RegisterBodyRequest): any;
    handleToken(accessToken:AccessToken):any;
    logout():void;
    isUserLoggedIn():boolean;
    getToken():AccessToken;
}

@Injectable()
export class AuthService implements IAuthController {

    constructor( private http:Http, private router:Router) {
        if(this.isUserLoggedIn()) {
            this.checkAuthorization(this.getToken());
        }
    }


    getToken():AccessToken {
        var token:AccessToken = JSON.parse(window.localStorage.getItem('token'));
        return token;
    }

    errorlist:string[];

    register(registerBodyRequest:RegisterBodyRequest):any {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post(Constants.BASE_URL + 'api/Account/Register', JSON.stringify(registerBodyRequest), {headers: headers});
    }

    login(loginBodyRequest:LoginBodyRequest):any {
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        var creds = "grant_type=password&username=" + loginBodyRequest.username + "&password=" + loginBodyRequest.password;
        return this.http.post(Constants.BASE_URL + 'token', creds, {headers: headers});
    }

    logout():void {
        window.localStorage.setItem('token', null);
        this.router.navigate(['/Main']);
    }

    handleToken(accessToken:AccessToken):any {
        window.localStorage.setItem('token', JSON.stringify(accessToken));
    }

    checkAuthorization(token:AccessToken):boolean {
        this.authorizationHelper(Constants.BASE_URL + 'api/Account/UserInfo', token)
            .map(res => res.json())
            .subscribe(
            data => {
            console.log('current token is valid')
            },
                err => {
                console.log('current token not valid, clearing token')
                window.localStorage.setItem('token', null);
            },
            () => console.log('Register complete')
        );
        return false;
    }

    private authorizationHelper(url:string, token:AccessToken): any {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append("Authorization", "Bearer " + token.access_token);
        return this.http.get(url, {headers: headers})
    }

    isUserLoggedIn():boolean {
        if (window.localStorage.getItem('token') == "null" || window.localStorage.getItem('token') == "undefined") {
            return false;
        }
        return true;
    }

}
