import { Pipe } from 'angular2/core';
import { TestMethod } from '../common/models';
import { TestMethodHelper } from '../utils/testMethodHelper';

@Pipe({ name: 'methodFullName' })
export class MethodFullNamePipe {
    transform(testMethod: TestMethod, args: string[]): any {
        return TestMethodHelper.extractMethodSignatureFromTestMethod(testMethod);
    }
}