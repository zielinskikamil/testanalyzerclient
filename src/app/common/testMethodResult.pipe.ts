import { Pipe } from 'angular2/core';

@Pipe({ name: 'methodResult' })
export class MethodResultPipe {
    transform(value: number, args: string[]): any {
        switch (value) {
            case 0:
                return 'Pass';
            case 1:
                return 'Fail';
            case 2:
                return 'Inconclusive';
            case 3:
                return 'Blocked';
            case 4:
                return 'Error';
            default:
                return 'Undefind Result';;
        }
    }
}