import { Pipe } from 'angular2/core';
import { ParsedTestMethodRow } from '../services/testComparerService';

@Pipe({ name: 'parsedTestMethodRowFlag' })
export class ParsedTestMethodRowFlafPipe {
    transform(parsedTestMethodRow: ParsedTestMethodRow, args: string[]): any {
        var className: string = '';
        if (parsedTestMethodRow.isAllPass) {
            className = 'success';
        } else if (parsedTestMethodRow.isAllFailed) {
            className = 'danger';
        } else if (!parsedTestMethodRow.isAllFailed && !parsedTestMethodRow.isAllPass) {
            // some must fail
            className = 'warning';
        }

        return className;
    }
}