export class ModelState {
    "":string[];
}

export class GenericResponse {
    constructor(public _body:any,
                public status:number,
                public statusText:string,
                public type:number,
                public url?:string) {
    }
}

export class ErrorBodyResponse {
    constructor(public Message:string,
                public ModelState:ModelState) {
    }
}

export class AuthenticationErrorBodyResponse {
    constructor(public error:string,
                public error_description:string) {
    }
}

export class LoginBodyRequest {
    constructor(public username:string,
                public password:string) {
    }
}

export class RegisterBodyRequest {
    constructor(public email:string,
                public password:string,
                public confirmPassword:string) {
    }
}

export class LoginData {
    constructor(public isLogged:boolean) {
    }
}

export class AccessToken {
    constructor(public access_token:string,
                public token_type:string,
                public expires_in:string,
                public userName:string,
                public issued:string,
                public expires:string) {
    }
}

export class SelectOption {
    Name: string;
    Id: number | string;
}

export class TestCycle {
    constructor(public Id: number,
                public Added: string,
                public Comment: string,
                public IsApproved: boolean,
                public Modified: string,
                public Name: string,
                public TestsRuns: TestRun[],
                public User: any) {
    }
}

export class MethodParameter {
    TestMethod: TestMethod;
    Name: string;
    Value: string;
    TestMethodId: number;
}

export class TestMethod {
    // constructor(
    Id:number;
    TestRun: TestRun;
    Name: string;
    MethodParameters: MethodParameter[];
    Result: any;
    ClassName: string;
    ErrorMsg: string;
    Duration: string;
    TestRunId: number;
    // ){ }
}

export class TestRun {
    constructor(public Id: number,
                public Added: string,
                public Modified: string,
                public Comment: string,
                public Name: string,
                public TestCycle: TestCycle,
                public TestCycleId: number | string,
                public User: any,
                public TestsMethods: TestMethod[],
                public UploadType: number,
                public Failed: number,
                public Succeed: number,
                public Other: number) {
    }
}

